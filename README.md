# ReactCalculatorAWS


This is a standalone example of how to build a react-front end that communicates with an AWS
API Gateway to access lambda functions.

All code is available, and this assumes you have already set up an RDS database service and
constructed 

## Use Cases

The system shall allow a user to add two values together. These values may be numeric or refer
to a previously defined constant. Constants can be created and removed.

* Add Values
* Create Constant
* Delete Constant

The capability to list all constants is useful. While not technically a use case, the React-based
GUI will update the GUI to contain all constants defined by the AWS backend.

## Lambda Functions

There are folders for the four envisioned lambda functions.

* adder which takes a POST { "arg1" : "val1", "arg2" : "val2" } and returns the sum of these two values.
* create which takes a POST { "name" : "constant-name", "value": "constant-numeric-value" } and creates a new constant with this name.
* delete which takes a POST { "name" : "constant-name" } and removes the constant.
* list which is the response to a GET request, and it returns the existing constants with their values.

Within each of these folders, you need to type "npm install" to properly install the mysql dependency within the `node_modules` directory.

Once created, zip and create the three files (node_modules, index,js, package.json) into a zip file which you can then upload to the implementation of the Lambda function.

### db_access layer

Each of these lambda functions depends on a `db_access` layer that contains the credentials to the RDS database. Be sure to add this layer to AWS and then make sure all lambda functions incorporate the layer as well.

### VPC Configuration

For these lambda functions to access the RDS securely, they must each be configured to run within
a VPC. Follow the instructions provided.

## React-based GUI

The app/ folder contains the front-end React code that uses native `fetch` invocations to access the AWS. There is an `Api.js` folder which contains the URL for the stage that was deployed in API gateway.

The code assumes the following AWS resources in the API Gateway:

* /calc takes a POST to invoke 'adder'
* /constants takes a GET to invoke 'list'
* /constants/create takes a POST to invoke 'create'
* /constants/delete takes a POST to invove 'delete'

You are expected to construct the necessary API gateway and Lambda functions in AWS

## S3 bucket

Once you have validated that the React application works locally, you should build it using `npm run build` and then this will construct a folder that you can copy to the S3 bucket.

Inside the React package.json file, there is a `homepage` attribute which is set to be relative

