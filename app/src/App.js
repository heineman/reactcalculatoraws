import './App.css'

import React from 'react'
import { addTwoNumbers } from './controller/AddTwoNumbers';
import { getConstants } from './controller/GetConstants';
import { createConstant } from './controller/CreateConstant';
import { removeConstant } from './controller/RemoveConstant';

function App() {
  const [redraw, forceRedraw] = React.useState(0);

  React.useEffect(()=>{
    getConstants()
  }, [redraw]);

  // this function requests the redraw, and can be passed as an argument to other functions
  const requestRedraw = () => {
    forceRedraw(redraw+1)
  }

  const createHandler = (e) => {
    createConstant(requestRedraw)
  }

  const removeHandler = (e) => {
    removeConstant(requestRedraw)
  }

  return (
    <div className="App">
    one: <input id="one" />
    two: <input id="two" />
    <button onClick={(e) => addTwoNumbers()}>Add</button><p></p>
    result: <input id="result" readOnly/>
    
    <h1>Constants</h1>
    name: <input id="constant-name"/>
    value: <input id="constant-value"/>
    <button onClick={createHandler}>Create</button>
    <button onClick={removeHandler}>Remove</button>
    <h2>Constants</h2>
    <div id="constant-list"></div>
  </div>
  );
}

export default App;
