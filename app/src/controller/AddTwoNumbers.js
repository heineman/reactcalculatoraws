import { post } from "./Api"

export function addTwoNumbers() {
    let arg1 = document.getElementById("one")
    let arg2 = document.getElementById("two")

    // prepare payload for the post
    let data = { 'arg1': arg1.value, 'arg2': arg2.value }

    const handler = (json) => {
        document.getElementById("result").value = json.body
    }

    post('/calc', data, handler)
  
}