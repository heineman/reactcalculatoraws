const mysql = require('mysql');
const db_access = require('/opt/nodejs/db_access')

exports.handler = async (event) => {
  
  // get credentials from the db_access layer (loaded separately via AWS console)
  var pool = mysql.createPool({
      host: db_access.config.host,
      user: db_access.config.user,
      password: db_access.config.password,
      database: db_access.config.database
  });

    let ClosePoll = (id) => {
      return new Promise((resolve, reject) => {
          pool.query("SELECT * from Polls WHERE id=?;", [id], (error, rows) => {
              if (error) { return reject(error); }
              if ((rows) && (rows.length == 1)) {
                   console.log(rows)
                  return resolve(rows);   // the auto numbered field can be extracted!
              } else {
                  return resolve([]);
              }
          });
      });
    }
      
  console.log(event)
  let close_result = await ClosePoll(event.id)
  console.log(close_result)
  let response = undefined
  if (close_result.length > 0) {
    response = {
      statusCode: 200,
      numTrue: close_result[0].numTrue,
      numFalse: close_result[0].numFalse,
      
      id: event.id
    }
  } else {
    response = {
      statusCode: 400,
      id: event.id
    }
  }
  pool.end();   // done with DB
  return response;
};
