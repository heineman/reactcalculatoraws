import { post } from "./Api"

export function createPoll(requestRedraw) {
    // potentially modify the model
    let q = document.getElementById("Q")

    // prepare payload for the post
    let data = { 'question' : q.value }

    const handler = (json) => {
        console.log(json)
        // clear inputs
        q.setAttribute('readonly', true)
        let idt = document.getElementById("ID")
        idt.value = json.id
        let results = document.getElementById("results")
        results.value = ""
    }

    post('/poll/create', data, handler)
}
