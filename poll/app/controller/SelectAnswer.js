import { post } from "./Api"

export function selectAnswer() {
    // potentially modify the model
    let id = document.getElementById("inputID")
    let choice = document.getElementById("choice")

     // prepare payload for the post
    let data = { 'id':id.value, 'answer':choice.value}
 
    const handler = (json) => {
        console.log(json)
        choice.value=""
    }

    post('/poll/select', data, handler)
}