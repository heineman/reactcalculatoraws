import { post } from "./Api"

export function closePoll(requestRedraw) {
    // potentially modify the model
   
    // prepare payload for the post
    let id = document.getElementById("ID")
    let data = { 'id': Number(id.value) }

    const handler = (json) => {
        console.log(json)
        // clear inputs
        let numt = json.numTrue
        let numf = json.numFalse
        id.value = ""
        let results = document.getElementById("results")
        if (numt > numf) {
            results.value = numt + " in favor, " + numf + " against"
        } else {
            results.value = numf + " against, " + numt + " in favor"
        }
    }

    post('/poll/close', data, handler)
}
