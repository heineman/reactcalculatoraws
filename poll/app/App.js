import './App.css'

import React from 'react'
import { createPoll } from './controller/CreatePoll';
import { closePoll } from './controller/ClosePoll';
import { selectAnswer } from './controller/SelectAnswer';

// If you just want to see the GUI from point of view of a student, set to true.
// If you want 
var showPoll = true

// If you want to see from student's point of view, set to true
var showStudent = true

function App() {

  const createPollHandler = (e) => {
   createPoll()
  }

  const closePollHandler = (e) => {
    closePoll()
  }

  const selectAnswerHandler = (e) => {
    selectAnswer()
  }

  return (
    <div className="App">
      { showPoll ?  (
      <div className="Outer"><br></br>
      <b>Question:</b> <input className="Quest" id="Q"></input><br></br><br></br>
     
      <button onClick={createPollHandler}>Create Poll</button>&nbsp;&nbsp;&nbsp;<button onClick={closePollHandler}>Close Poll</button>
      <br></br><br></br><label>Use Poll ID: <input id="ID" readOnly></input></label>
      <br></br><br></br><label>Results: <input id="results" readOnly></input></label>
      
      <br></br><br></br>
      </div>
      ) : null}

      { showStudent ?  (
        <div>
          <hr></hr>
          <b>Poll ID:&nbsp;&nbsp;</b><input id="inputID"></input><br></br>
          <b>True</b> or <b>False:</b>&nbsp;&nbsp;<input id="choice"></input><br></br><br></br>
          <button onClick={selectAnswerHandler}>Select</button>
          <div id="constant-list"></div>
        </div>) : null
        }
  </div>
  );
}

export default App;
