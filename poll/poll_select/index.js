const mysql = require('mysql');
const db_access = require('/opt/nodejs/db_access')

exports.handler = async (event) => {
    
    // get credentials from the db_access layer (loaded separately via AWS console)
    var pool = mysql.createPool({
      host: db_access.config.host,
      user: db_access.config.user,
      password: db_access.config.password,
      database: db_access.config.database
    });
    
    // NOTE: cannot use ? as wildcard for fields
    // so have to use javascript ` string interpolation ``    
    let IncrementField = (field, id) => {        
      return new Promise((resolve, reject) => {
        pool.query(`UPDATE Polls SET num${field}=num${field}+1 WHERE id=?`, [id],
           (error, rows) => {
              if (error) { return reject(error); }
              if ((rows) && (rows.affectedRows == 1)) {
                return resolve(true);   // TRUE if does exist 
              } else {
                return resolve(false);   // REJECT if couldn't update 
              }
           });
       });
    }

  let ans =event.answer.toLowerCase()
  let response = undefined
    
  if (ans === "true" || ans === "false") {
  
    try {
      if (ans === "true" ) { ans = "True" } else { ans = "False" }
      const up = await IncrementField(ans, event.id);
      if (up) {
        response = {
          statusCode: 200,
          success: true
        } 
      } else {
        response = {
          statusCode: 400,
          success: false
        }
      }
    } catch (err)  {
      response = {
        statusCode: 400,
        success: false,
        error: err
      }
    }
  } else {
    response = {
        statusCode: 400,
        success: false,
        error: "unknown selection: " + ans
      }
  }
  return response;
};
