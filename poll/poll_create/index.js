const mysql = require('mysql');
const db_access = require('/opt/nodejs/db_access')

exports.handler = async (event) => {
  
  // get credentials from the db_access layer (loaded separately via AWS console)
  var pool = mysql.createPool({
      host: db_access.config.host,
      user: db_access.config.user,
      password: db_access.config.password,
      database: db_access.config.database
  });

    let CreatePoll = (q) => {
      return new Promise((resolve, reject) => {
          pool.query("INSERT into Polls(Question,numTrue,numFalse) VALUES(?,0,0);", [q], (error, rows) => {
              if (error) { return reject(error); }
              if ((rows) && (rows.affectedRows == 1)) {
                   console.log(rows)
                  return resolve(rows.insertId);   // the auto numbered field can be extracted!
              } else {
                  return resolve(false);
              }
          });
      });
    }
      
  let add_result = await CreatePoll(event.question)
  let response = {
    statusCode: 200,
    
    id: add_result
  }
  
  pool.end();   // done with DB
  return response;
};
